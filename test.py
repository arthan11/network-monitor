import subprocess
from platform import system as system_name # Returns the system/OS name
#from os import system as system_call       # Execute a shell command
from time import sleep
from datetime import datetime, timedelta

def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that some hosts may not respond to a ping request even if the host name is valid.
    """

    # Ping parameters as function of OS
    parameters = "-n 1" if system_name().lower()=="windows" else "-c 1"

    # Pinging
    #return system_call("ping " + parameters + " " + host) == 0
    ping_response = subprocess.Popen(["ping", "-n", "1", "-w", "1000", host], stdout=subprocess.PIPE).stdout.read()
    avg_pos = ping_response.find('Average = ')
    avg = 0
    if avg_pos > -1:
        avg_pos2 = ping_response.find('ms', avg_pos)
        avg = ping_response[avg_pos+10:avg_pos2]
        avg = int(avg)
    return avg
    
if __name__ == '__main__':
    online = True
    time1 = datetime.now() - timedelta(seconds=5)
    print time1
    while True:
        time2 = datetime.now()
        delta = time2 - time1
        if delta.seconds > 4:
            time1 = datetime.now()
            ping1 = ping('google.pl')
            ping2 = ping('speedcenter.level3.net')
            ping3 = ping('opendns.com')
            
            online_now = (ping1 + ping2 + ping3 > 0)
            
            if online_now != online:
                status = '{}: {}'.format(time2, online_now)   
                print status
                with open('log.txt', 'a+') as f:
                    f.write(status+'\n') 
                    f.close()
                online = online_now
            
        else:
            sleep(1)